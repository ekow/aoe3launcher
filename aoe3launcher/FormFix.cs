﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace aoe3launcher
{
  public partial class FormFix : Form
  {
    private readonly string dxFile;
    private readonly string msxmlFile;
    private readonly bool existDx;
    private readonly bool existMsXml;

    public FormFix()
    {
      InitializeComponent();

      dxFile = $"{FormMain.LocalPath}\\directx9\\DXSETUP.exe";
      msxmlFile = $"{FormMain.LocalPath}\\msxml.msi";

      if (!File.Exists(msxmlFile))
        msxmlFile = $"{FormMain.LocalPath}\\directx9\\msxml.msi";

      existDx = File.Exists(dxFile);
      existMsXml = File.Exists(msxmlFile);
    }

    private void buttonRegistry_Click(object sender, EventArgs e)
    {
      RegistryFix.Fix();
    }

    private void buttonDx9_Click(object sender, EventArgs e)
    {
      if (!existDx)
      {
        MessageBox.Show("Can't find DirectX installer, please try manually.", "Error", MessageBoxButtons.OK,
          MessageBoxIcon.Error);
        return;
      }

      try
      {
        Process process = Process.Start($"{dxFile}");
        process?.WaitForExit();
      }
      catch
      {
        MessageBox.Show("DirectX installation failed, please try manually.", "Error", MessageBoxButtons.OK,
          MessageBoxIcon.Error);
      }
    }

    private void buttonMsXml_Click(object sender, EventArgs e)
    {
      if (!existMsXml)
      {
        MessageBox.Show("Can't find MSXML installer, please try manually.", "Error", MessageBoxButtons.OK,
          MessageBoxIcon.Error);
        return;
      }

      try
      {
        Process process = Process.Start("msiexec.exe", $"/qb \"{msxmlFile}\"");
        process?.WaitForExit();
      }
      catch
      {
        MessageBox.Show("MSXML installation failed, please try manually.", "Error", MessageBoxButtons.OK,
          MessageBoxIcon.Error);
      }
    }
  }
}