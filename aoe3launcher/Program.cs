﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace aoe3launcher
{
  internal static class Program
  {
    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    [STAThread]
    private static void Main()
    {
      //AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(CustomAssemblyResolve);

      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);
      Application.Run(new FormMain());
    }

    private static Assembly CustomAssemblyResolve(object sender, ResolveEventArgs args)
    {
      string assemblyName = new AssemblyName(args.Name).Name;
      string resourceName = string.Format("{0}.Libs.{1}.dll", Assembly.GetExecutingAssembly().GetName().Name, assemblyName);
      using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName))
      {
        Debug.Assert(stream != null, string.Format("Required Assembly resource {0} not found or null!", resourceName));
        var assemblyData = new byte[stream.Length];
        stream.Read(assemblyData, 0, assemblyData.Length);
        return Assembly.Load(assemblyData);
      }
    }
  }
}