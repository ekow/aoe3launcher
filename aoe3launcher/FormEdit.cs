﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using System.Xml;

namespace aoe3launcher
{
  public partial class FormEdit : Form
  {
    private const string EleCiv = "civ";
    private const string EleCity = "name";
    private const string EleHero = "heroname";
    private const string EleLevel = "level";
    private const string EleCards = "skillpoints";
    private const string SavePrefix = "sp_";
    private const string SavePostfix = "_homecity";
    private const string SaveExtension = ".xml";

    private class SaveGame
    {
      public string FileName;
      public string Civ;
      public string City;
      public string Hero;
      public int Level;
      public int Cards;
      public XmlDocument Xml;
    }

    private readonly Dictionary<int, SaveGame> savegames = new Dictionary<int, SaveGame>();
    private int selectedIdx;
    private SaveGame selected;
    private bool changed;
    private readonly string saveFolder;

    public FormEdit()
    {
      saveFolder = $"{Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}\\My Games\\Age of Empires 3\\Savegame";
      InitializeComponent();
    }

    public void Clear()
    {
      listBox1.Items.Clear();
      savegames.Clear();
    }

    private void RefreshCities()
    {
      savegames.Clear();
      listBox1.Items.Clear();
      groupBox1.Text = "HomeCity";
      groupBox1.Enabled = false;
      changed = false;
      buttonSave.Enabled = false;
      selectedIdx = -1;

      if (Directory.Exists(saveFolder))
      {
        string[] files = Directory.GetFiles(saveFolder, $"{SavePrefix}*{SavePostfix}{SaveExtension}");
        for (int i = 0; i < files.Length; i++)
        {
          string file = files[i];
          if (File.Exists(file))
          {
            Debug.WriteLine(file);
            XmlDocument doc = new XmlDocument();
            doc.Load(file);
            XmlNode hCity = doc.DocumentElement?[EleCity];
            XmlNode hCiv = doc.DocumentElement?[EleCiv];
            XmlNode hHero = doc.DocumentElement?[EleHero];
            XmlNode hLevel = doc.DocumentElement?[EleLevel];
            XmlNode hCards = doc.DocumentElement?[EleCards];
            if (hCity != null && hCiv != null && hHero != null && hLevel != null && hCards != null)
            {
              int level;
              int cards;
              int.TryParse(hLevel.InnerText, out level);
              int.TryParse(hCards.InnerText, out cards);
              int idx = listBox1.Items.Add(hCity.InnerText);
              savegames[idx] = new SaveGame()
              {
                FileName = file,
                Xml = doc,
                Civ = hCiv.InnerText,
                City = hCity.InnerText,
                Hero = hHero.InnerText,
                Level = level,
                Cards = cards
              };
            }
          }
          else
          {
            Debug.WriteLine(file);
          }
        }
      }
    }

    private void Save(SaveGame save)
    {
      XmlNode hCity = save.Xml.DocumentElement?[EleCity];
      XmlNode hCiv = save.Xml.DocumentElement?[EleCiv];
      XmlNode hHero = save.Xml.DocumentElement?[EleHero];
      XmlNode hLevel = save.Xml.DocumentElement?[EleLevel];
      XmlNode hCards = save.Xml.DocumentElement?[EleCards];
      if (hCity != null && hCiv != null && hHero != null && hLevel != null && hCards != null)
      {
        hCity.InnerText = save.City;
        hHero.InnerText = save.Hero;
        hLevel.InnerText = save.Level.ToString();
        hCards.InnerText = save.Cards.ToString();

        if (File.Exists(save.FileName))
          File.Delete(save.FileName);

        string newFileName = $"{saveFolder}\\{SavePrefix}{save.City}{SavePostfix}{SaveExtension}";
        save.FileName = newFileName;
        save.Xml.Save(save.FileName);

        listBox1.SelectedIndexChanged -= listBox1_SelectedIndexChanged;
        listBox1.Items[selectedIdx] = save.City;
        listBox1.SelectedIndex = selectedIdx;
        listBox1.SelectedIndexChanged += listBox1_SelectedIndexChanged;
      }
    }

    private SaveGame CheckName(string name)
    {
      foreach (var sg in savegames)
      {
        if (sg.Value.City.Equals(name))
          return sg.Value;
      }
      return null;
    }

    private void FormEdit_Load(object sender, EventArgs e)
    {
      RefreshCities();
    }

    private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (selectedIdx == listBox1.SelectedIndex)
        return;

      if (changed &&
          MessageBox.Show("Discard changes?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
          DialogResult.No)
      {
        listBox1.SelectedIndexChanged -= listBox1_SelectedIndexChanged;
        listBox1.SelectedIndex = selectedIdx;
        listBox1.SelectedIndexChanged += listBox1_SelectedIndexChanged;
        return;
      }

      selectedIdx = listBox1.SelectedIndex;
      if (savegames.ContainsKey(selectedIdx))
      {
        SaveGame save = savegames[selectedIdx];
        selected = save;
        groupBox1.Enabled = true;
        groupBox1.Text = save.Civ;
        textBoxCity.Text = save.City;
        textBoxHero.Text = save.Hero;
        textBoxLevel.Text = save.Level.ToString();
        textBoxCards.Text = save.Cards.ToString();

        changed = false;
        buttonSave.Enabled = false;
      }
    }

    private void buttonCancel_Click(object sender, EventArgs e)
    {
      if (changed && MessageBox.Show("Discard changes?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
        Close();
      else if (!changed)
        Close();
    }

    private void texts_TextChanged(object sender, EventArgs e)
    {
      if (selected != null)
      {
        changed = true;
        buttonSave.Enabled = true;
      }
    }

    private void buttonReload_Click(object sender, EventArgs e)
    {
      if (changed && MessageBox.Show("Discard changes?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
        RefreshCities();
      else if (!changed)
        RefreshCities();
    }

    private void buttonSave_Click(object sender, EventArgs e)
    {
      if (selected != null)
      {
        int level;
        int cards;
        int.TryParse(textBoxLevel.Text, out level);
        int.TryParse(textBoxCards.Text, out cards);

        SaveGame sg;

        if (textBoxCity.Text.Length == 0 || textBoxHero.Text.Length == 0)
          MessageBox.Show("Name cannot be empty!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        else if ((sg = CheckName(textBoxCity.Text)) != selected && sg != null)
          MessageBox.Show("Name already used!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        else if (level <= 0)
          MessageBox.Show("Level must be greater than 0!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        else if (cards <= 0)
          MessageBox.Show("Cards must be greater than 0!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        else
        {
          selected.City = textBoxCity.Text;
          selected.Hero = textBoxHero.Text;
          selected.Level = level;
          selected.Cards = cards;
          Save(selected);
          changed = false;
          buttonSave.Enabled = false;
        }
      }
    }
  }
}