﻿namespace aoe3launcher
{
  partial class FormEdit
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.label1 = new System.Windows.Forms.Label();
      this.listBox1 = new System.Windows.Forms.ListBox();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.label5 = new System.Windows.Forms.Label();
      this.textBoxCity = new System.Windows.Forms.TextBox();
      this.textBoxCards = new System.Windows.Forms.TextBox();
      this.textBoxLevel = new System.Windows.Forms.TextBox();
      this.textBoxHero = new System.Windows.Forms.TextBox();
      this.label4 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.buttonCancel = new System.Windows.Forms.Button();
      this.buttonReload = new System.Windows.Forms.Button();
      this.buttonSave = new System.Windows.Forms.Button();
      this.groupBox1.SuspendLayout();
      this.SuspendLayout();
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(9, 9);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(59, 13);
      this.label1.TabIndex = 0;
      this.label1.Text = "Homecities";
      // 
      // listBox1
      // 
      this.listBox1.FormattingEnabled = true;
      this.listBox1.Location = new System.Drawing.Point(12, 25);
      this.listBox1.Name = "listBox1";
      this.listBox1.Size = new System.Drawing.Size(129, 225);
      this.listBox1.TabIndex = 0;
      this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.label5);
      this.groupBox1.Controls.Add(this.textBoxCity);
      this.groupBox1.Controls.Add(this.textBoxCards);
      this.groupBox1.Controls.Add(this.textBoxLevel);
      this.groupBox1.Controls.Add(this.textBoxHero);
      this.groupBox1.Controls.Add(this.label4);
      this.groupBox1.Controls.Add(this.label3);
      this.groupBox1.Controls.Add(this.label2);
      this.groupBox1.Location = new System.Drawing.Point(147, 14);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(218, 209);
      this.groupBox1.TabIndex = 1;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "HomeCity";
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(7, 50);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(30, 13);
      this.label5.TabIndex = 4;
      this.label5.Text = "Hero";
      // 
      // textBoxCity
      // 
      this.textBoxCity.Location = new System.Drawing.Point(77, 19);
      this.textBoxCity.Name = "textBoxCity";
      this.textBoxCity.Size = new System.Drawing.Size(123, 20);
      this.textBoxCity.TabIndex = 3;
      this.textBoxCity.TextChanged += new System.EventHandler(this.texts_TextChanged);
      // 
      // textBoxCards
      // 
      this.textBoxCards.Location = new System.Drawing.Point(157, 99);
      this.textBoxCards.Name = "textBoxCards";
      this.textBoxCards.Size = new System.Drawing.Size(37, 20);
      this.textBoxCards.TabIndex = 9;
      this.textBoxCards.Text = "1";
      this.textBoxCards.TextChanged += new System.EventHandler(this.texts_TextChanged);
      // 
      // textBoxLevel
      // 
      this.textBoxLevel.Location = new System.Drawing.Point(157, 73);
      this.textBoxLevel.Name = "textBoxLevel";
      this.textBoxLevel.Size = new System.Drawing.Size(37, 20);
      this.textBoxLevel.TabIndex = 7;
      this.textBoxLevel.Text = "1";
      this.textBoxLevel.TextChanged += new System.EventHandler(this.texts_TextChanged);
      // 
      // textBoxHero
      // 
      this.textBoxHero.Location = new System.Drawing.Point(77, 47);
      this.textBoxHero.Name = "textBoxHero";
      this.textBoxHero.Size = new System.Drawing.Size(123, 20);
      this.textBoxHero.TabIndex = 5;
      this.textBoxHero.TextChanged += new System.EventHandler(this.texts_TextChanged);
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(7, 102);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(82, 13);
      this.label4.TabIndex = 8;
      this.label4.Text = "Cards remaining";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(7, 76);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(33, 13);
      this.label3.TabIndex = 6;
      this.label3.Text = "Level";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(7, 22);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(24, 13);
      this.label2.TabIndex = 2;
      this.label2.Text = "City";
      // 
      // buttonCancel
      // 
      this.buttonCancel.Location = new System.Drawing.Point(293, 227);
      this.buttonCancel.Name = "buttonCancel";
      this.buttonCancel.Size = new System.Drawing.Size(72, 23);
      this.buttonCancel.TabIndex = 12;
      this.buttonCancel.Text = "Cancel";
      this.buttonCancel.UseVisualStyleBackColor = true;
      this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
      // 
      // buttonReload
      // 
      this.buttonReload.Location = new System.Drawing.Point(147, 227);
      this.buttonReload.Name = "buttonReload";
      this.buttonReload.Size = new System.Drawing.Size(73, 23);
      this.buttonReload.TabIndex = 10;
      this.buttonReload.Text = "Reload";
      this.buttonReload.UseVisualStyleBackColor = true;
      this.buttonReload.Click += new System.EventHandler(this.buttonReload_Click);
      // 
      // buttonSave
      // 
      this.buttonSave.Location = new System.Drawing.Point(226, 227);
      this.buttonSave.Name = "buttonSave";
      this.buttonSave.Size = new System.Drawing.Size(61, 23);
      this.buttonSave.TabIndex = 11;
      this.buttonSave.Text = "Save";
      this.buttonSave.UseVisualStyleBackColor = true;
      this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
      // 
      // FormEdit
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(377, 260);
      this.Controls.Add(this.buttonSave);
      this.Controls.Add(this.buttonReload);
      this.Controls.Add(this.buttonCancel);
      this.Controls.Add(this.groupBox1);
      this.Controls.Add(this.listBox1);
      this.Controls.Add(this.label1);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
      this.Name = "FormEdit";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Edit Homecities";
      this.Load += new System.EventHandler(this.FormEdit_Load);
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.ListBox listBox1;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.Button buttonCancel;
    private System.Windows.Forms.Button buttonReload;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.TextBox textBoxCards;
    private System.Windows.Forms.TextBox textBoxLevel;
    private System.Windows.Forms.TextBox textBoxHero;
    private System.Windows.Forms.Button buttonSave;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.TextBox textBoxCity;
  }
}