﻿namespace aoe3launcher
{
  partial class FormFix
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.buttonRegistry = new System.Windows.Forms.Button();
      this.buttonDx9 = new System.Windows.Forms.Button();
      this.buttonMsXml = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // buttonRegistry
      // 
      this.buttonRegistry.Location = new System.Drawing.Point(12, 12);
      this.buttonRegistry.Name = "buttonRegistry";
      this.buttonRegistry.Size = new System.Drawing.Size(123, 23);
      this.buttonRegistry.TabIndex = 0;
      this.buttonRegistry.Text = "Fix Registry";
      this.buttonRegistry.UseVisualStyleBackColor = true;
      this.buttonRegistry.Click += new System.EventHandler(this.buttonRegistry_Click);
      // 
      // buttonDx9
      // 
      this.buttonDx9.Location = new System.Drawing.Point(12, 41);
      this.buttonDx9.Name = "buttonDx9";
      this.buttonDx9.Size = new System.Drawing.Size(123, 23);
      this.buttonDx9.TabIndex = 1;
      this.buttonDx9.Text = "Install DirectX 9";
      this.buttonDx9.UseVisualStyleBackColor = true;
      this.buttonDx9.Click += new System.EventHandler(this.buttonDx9_Click);
      // 
      // buttonMsXml
      // 
      this.buttonMsXml.Location = new System.Drawing.Point(12, 70);
      this.buttonMsXml.Name = "buttonMsXml";
      this.buttonMsXml.Size = new System.Drawing.Size(123, 23);
      this.buttonMsXml.TabIndex = 2;
      this.buttonMsXml.Text = "Install MSXML";
      this.buttonMsXml.UseVisualStyleBackColor = true;
      this.buttonMsXml.Click += new System.EventHandler(this.buttonMsXml_Click);
      // 
      // FormFix
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(147, 101);
      this.Controls.Add(this.buttonMsXml);
      this.Controls.Add(this.buttonDx9);
      this.Controls.Add(this.buttonRegistry);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
      this.Name = "FormFix";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Fix";
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Button buttonRegistry;
    private System.Windows.Forms.Button buttonDx9;
    private System.Windows.Forms.Button buttonMsXml;
  }
}