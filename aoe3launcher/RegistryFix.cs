﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace aoe3launcher
{
  public class RegistryFix
  {
    private const string RegExe = "regedit.exe";
    private const string RegFile = "_aoe3.reg";

    static readonly bool Is64BitProcess = (IntPtr.Size == 8);
    static readonly bool Is64BitOperatingSystem = Is64BitProcess || InternalCheckIsWow64();

    [DllImport("kernel32.dll", SetLastError = true, CallingConvention = CallingConvention.Winapi)]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool IsWow64Process(
        [In] IntPtr hProcess,
        [Out] out bool wow64Process
    );

    public static bool InternalCheckIsWow64()
    {
      if ((Environment.OSVersion.Version.Major == 5 && Environment.OSVersion.Version.Minor >= 1) ||
          Environment.OSVersion.Version.Major >= 6)
      {
        using (Process p = Process.GetCurrentProcess())
        {
          bool retVal;
          if (!IsWow64Process(p.Handle, out retVal))
          {
            return false;
          }
          return retVal;
        }
      }
      else
      {
        return false;
      }
    }

    public static void Write()
    {
      string path = FormMain.LocalPath.Replace("\\", "\\\\");
      string win64 = Is64BitOperatingSystem ? "Wow6432Node\\" : "";
      StringBuilder regString = new StringBuilder();

      // aoe3
      regString.AppendLine(@"Windows Registry Editor Version 5.00");
      regString.AppendLine();
      regString.AppendLine($@"[HKEY_LOCAL_MACHINE\SOFTWARE\{win64}Microsoft\Microsoft Games]");
      regString.AppendLine();
      regString.AppendLine($@"[HKEY_LOCAL_MACHINE\SOFTWARE\{win64}Microsoft\Microsoft Games\Age of Empires 3]");
      regString.AppendLine();
      regString.AppendLine($@"[HKEY_LOCAL_MACHINE\SOFTWARE\{win64}Microsoft\Microsoft Games\Age of Empires 3\1.0]");
      regString.AppendLine("\"PID\"=\"77076-341-4893731-40596\"");
      regString.AppendLine("\"DigitalProductID\"=hex:a4,00,00,00,03,00,00,00,37,37,30,37,36,2d,33,34,31,2d,\\");
      regString.AppendLine("  34,38,39,33,37,33,31,2d,34,30,35,39,36,00,50,00,00,00,58,31,33,2d,36,36,32,\\");
      regString.AppendLine("  38,36,00,00,00,00,00,00,00,aa,c2,20,fa,4b,da,a3,aa,43,5f,21,ef,a2,71,02,00,\\");
      regString.AppendLine("  00,00,00,00,b0,55,cf,55,40,f6,69,03,00,00,00,00,00,00,00,00,00,00,00,00,00,\\");
      regString.AppendLine("  00,00,00,00,00,00,00,00,00,00,00,34,37,35,30,34,00,00,00,00,00,00,00,dd,14,\\");
      regString.AppendLine("  00,00,3f,9b,84,da,00,08,00,00,87,01,00,00,00,00,00,00,00,00,00,00,00,00,00,\\");
      regString.AppendLine("  00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,cb,d9,f0,84");
      regString.AppendLine("\"LangID\"=dword:00000409");
      regString.AppendLine("\"CDPath\" = \"X:\\\\\"");
      regString.AppendLine("\"Version\"=\"1.14\"");
      regString.AppendLine($"\"SetupPath\"=\"{path}\\\\\"");
      regString.AppendLine("\"CheckAccess\"=\"1\"");
      regString.AppendLine();

      // aoe3x
      regString.AppendLine($@"[HKEY_LOCAL_MACHINE\SOFTWARE\{win64}Microsoft\Microsoft Games\Age of Empires 3 Expansion Pack]");
      regString.AppendLine();
      regString.AppendLine($@"[HKEY_LOCAL_MACHINE\SOFTWARE\{win64}Microsoft\Microsoft Games\Age of Empires 3 Expansion Pack\1.0]");
      regString.AppendLine("\"PID\"=\"84807-668-8529576-60693\"");
      regString.AppendLine("\"DigitalProductID\"=hex:a4,00,00,00,03,00,00,00,38,34,38,30,37,2d,36,36,38,2d,\\");
      regString.AppendLine("  38,35,32,39,35,37,36,2d,36,30,36,39,33,00,78,00,00,00,44,46,37,2d,30,30,30,\\");
      regString.AppendLine("  30,31,00,00,00,00,00,00,00,38,5d,cb,26,45,7e,60,c8,30,ba,32,26,51,dd,01,00,\\");
      regString.AppendLine("  00,00,00,00,41,57,cf,55,89,14,70,03,00,00,00,00,00,00,00,00,00,00,00,00,00,\\");
      regString.AppendLine("  00,00,00,00,00,00,00,00,00,00,00,34,37,35,30,34,00,00,00,00,00,00,00,00,00,\\");
      regString.AppendLine("  00,00,3f,9b,84,da,00,08,00,00,87,01,00,00,00,00,00,00,00,00,00,00,00,00,00,\\");
      regString.AppendLine("  00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,ef,54,c4,2f");
      regString.AppendLine("\"LangID\"=dword:00000409");
      regString.AppendLine("\"CDPath\" = \"X:\\\\\"");
      regString.AppendLine("\"Version\"=\"1.06\"");
      regString.AppendLine($"\"SetupPath\"=\"{path}\\\\\"");
      regString.AppendLine("\"CheckAccess\"=\"1\"");
      regString.AppendLine();

      // aoe3y
      regString.AppendLine($@"[HKEY_LOCAL_MACHINE\SOFTWARE\{win64}Microsoft\Microsoft Games\Age of Empires 3 Expansion Pack 2]");
      regString.AppendLine();
      regString.AppendLine($@"[HKEY_LOCAL_MACHINE\SOFTWARE\{win64}Microsoft\Microsoft Games\Age of Empires 3 Expansion Pack 2\1.0]");
      regString.AppendLine("\"PID\"=\"84807-701-1229304-60562\"");
      regString.AppendLine("\"DigitalProductID\"=hex:a4,00,00,00,03,00,00,00,38,34,38,30,37,2d,37,30,31,2d,\\");
      regString.AppendLine("  31,32,32,39,33,30,34,2d,36,30,35,36,32,00,78,00,00,00,58,31,33,2d,38,30,36,\\");
      regString.AppendLine("  31,30,00,00,00,00,00,00,00,7b,7d,fb,75,39,ee,40,ad,96,4d,e5,d7,11,b1,02,00,\\");
      regString.AppendLine("  00,00,00,00,b7,57,cf,55,29,e4,71,03,01,00,00,00,00,00,00,00,00,00,00,00,00,\\");
      regString.AppendLine("  00,00,00,00,00,00,00,00,00,00,00,34,37,35,30,34,00,00,00,00,00,00,00,00,00,\\");
      regString.AppendLine("  00,00,3f,9b,84,da,00,08,00,00,87,01,00,00,00,00,00,00,00,00,00,00,00,00,00,\\");
      regString.AppendLine("  00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,00,fb,b3,a5,0b");
      regString.AppendLine("\"DoubleHash\"=hex:48,5b,cc,c6,84,97,4d,aa");
      regString.AppendLine("\"LangID\"=dword:00000409");
      regString.AppendLine("\"CDPath\" = \"X:\\\\\"");
      regString.AppendLine("\"Version\"=\"1.03\"");
      regString.AppendLine($"\"SetupPath\"=\"{path}\\\\\"");
      regString.AppendLine("\"CheckAccess\"=\"1\"");
      regString.AppendLine();

      File.WriteAllText($"{FormMain.LocalPath}\\{RegFile}", regString.ToString());
    }

    public static void Fix()
    {
      try
      {
        Write();
        Process regeditProcess = Process.Start(RegExe, $"/s {RegFile}");
        regeditProcess?.WaitForExit();
      }
      catch
      {
        MessageBox.Show("Failed! Please try manually.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
      finally
      {
        string file = $"{FormMain.LocalPath}\\{RegFile}";
        if (File.Exists(file))
        File.Delete(file);
      }
    }
  }
}