﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;

namespace aoe3launcher
{
  public partial class FormMain : Form
  {
    public static string LocalPath;
    private const string DefaultExe = "age3y.exe";

    private string exeName;
    private int selected;
    private bool skip;
    private readonly string fileConfig;
    private readonly FormEdit editForm;
    private readonly FormFix fixForm;

    public FormMain()
    {
      InitializeComponent();

      exeName = DefaultExe;
      LocalPath = Path.GetDirectoryName(new Uri(System.Reflection.Assembly.GetExecutingAssembly().CodeBase).LocalPath);

      fileConfig =
        $"{LocalPath}\\{System.Reflection.Assembly.GetExecutingAssembly().GetName().Name}.cfg";

      editForm = new FormEdit();
      fixForm = new FormFix();

      LoadConfig();
    }

    public void RefreshIpAddresses()
    {
      IPAddress[] localIPs = Dns.GetHostAddresses(Dns.GetHostName());

      int last = listBoxIpAddresses.SelectedIndex;
      listBoxIpAddresses.Items.Clear();

      for (int i = 0, c = localIPs.Length; i < c; i++)
      {
        IPAddress ip = localIPs[i];
        string s = ip.ToString();
        if (IPAddress.Parse(s).AddressFamily == AddressFamily.InterNetwork)
          listBoxIpAddresses.Items.Add(s);
      }

      if (radioButtonSelectIp.Checked && listBoxIpAddresses.Items.Count > 0)
      {
        if (last >= 0 && last < listBoxIpAddresses.Items.Count)
          listBoxIpAddresses.SelectedIndex = last;
        else
        {
          listBoxIpAddresses.SelectedIndex = 0;
        }
      }
      else
        radioButtonLastIp.Checked = true;
    }

    private void LoadConfig()
    {
      if (File.Exists(fileConfig))
      {
        // default value
        textBoxLastIp.Text = "0.0.0.0";
        skip = true;
        checkBoxSkip.Checked = skip;

        using (StreamReader sr = File.OpenText(fileConfig))
        {
          string s;
          int count = 0;
          while ((s = sr.ReadLine()) != null)
          {
            count++;
            switch (count)
            {
              case 1:
                textBoxLastIp.Text = s;
                break;

              case 2:
                exeName = s.Length == 0 ? DefaultExe : s;
                break;

              case 3:
                skip = s.Equals("1");
                checkBoxSkip.Checked = skip;
                break;
            }
          }
        }
      }
      else
      {
        textBoxLastIp.Text = "0.0.0.0";
      }
    }

    private void SaveConfig(string lastIp)
    {
      StringBuilder cfg = new StringBuilder();
      cfg.AppendLine(lastIp);
      cfg.AppendLine(exeName);
      cfg.AppendLine(skip ? "1" : "0");
      File.WriteAllText(fileConfig, cfg.ToString());
    }

    private void radioButtonLastIp_CheckedChanged(object sender, EventArgs e)
    {
      selected = 0;
    }

    private void radioButtonSelectIp_CheckedChanged(object sender, EventArgs e)
    {
      selected = 1;
    }

    private void Form1_Load(object sender, EventArgs e)
    {
      Text = $"{Application.ProductName} ({Application.ProductVersion})";
      RefreshIpAddresses();
    }

    private void buttonRefresh_Click(object sender, EventArgs e)
    {
      RefreshIpAddresses();
    }

    private void buttonLaunch_Click(object sender, EventArgs e)
    {
      var selectedIp = selected == 1 ? listBoxIpAddresses.SelectedItem.ToString() : textBoxLastIp.Text;

      StringBuilder args = new StringBuilder();
      args.Append($"OverrideAddress=\"{selectedIp}\"");
      if (skip)
        args.Append(" +noIntroCinematics");

      ProcessStartInfo aoe = new ProcessStartInfo
      {
        Arguments = args.ToString(),
        FileName = exeName
      };

      SaveConfig(selectedIp);
      try
      {
        Process proc = Process.Start(aoe);
      }
      catch (Exception ex)
      {
        MessageBox.Show(ex.Message);
      }
    }

    private void textBoxLastIp_Click(object sender, EventArgs e)
    {
      radioButtonLastIp.Checked = true;
    }

    private void textBoxLastIp_TextChanged(object sender, EventArgs e)
    {
      radioButtonLastIp.Checked = true;
    }

    private void listBoxIpAddresses_SelectedIndexChanged(object sender, EventArgs e)
    {
      radioButtonSelectIp.Checked = true;
    }

    private void checkBoxSkip_CheckedChanged(object sender, EventArgs e)
    {
      skip = checkBoxSkip.Checked;
    }

    private void tools_MouseLeave(object sender, EventArgs e)
    {
      toolStripStatusLabel1.Text = "Ready";
    }

    private void buttonLaunch_MouseEnter(object sender, EventArgs e)
    {
      toolStripStatusLabel1.Text = "Launch game";
    }

    private void listBoxIpAddresses_MouseEnter(object sender, EventArgs e)
    {
      toolStripStatusLabel1.Text = "Select override ip address";
    }

    private void buttonRefresh_MouseEnter(object sender, EventArgs e)
    {
      toolStripStatusLabel1.Text = "Refresh ip address list";
    }

    private void textBoxLastIp_MouseEnter(object sender, EventArgs e)
    {
      toolStripStatusLabel1.Text = "Type override ip address";
    }

    private void checkBoxSkip_MouseEnter(object sender, EventArgs e)
    {
      toolStripStatusLabel1.Text = "Skip cinematic intro";
    }

    private void buttonEdit_MouseEnter(object sender, EventArgs e)
    {
      toolStripStatusLabel1.Text = "Edit Homecities";
    }

    private void buttonEdit_Click(object sender, EventArgs e)
    {
      editForm.ShowDialog();
      editForm.Clear();
    }

    private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
    {
      SaveConfig(selected == 1 ? listBoxIpAddresses.SelectedItem.ToString() : textBoxLastIp.Text);
    }

    private void buttonSetup_MouseEnter(object sender, EventArgs e)
    {
      toolStripStatusLabel1.Text = "Fix registry, install DirectX 9 or MSXML";
    }

    private void buttonSetup_Click(object sender, EventArgs e)
    {
      fixForm.ShowDialog();
    }
  }
}