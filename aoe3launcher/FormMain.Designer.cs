﻿namespace aoe3launcher
{
  partial class FormMain
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
      this.radioButtonLastIp = new System.Windows.Forms.RadioButton();
      this.radioButtonSelectIp = new System.Windows.Forms.RadioButton();
      this.buttonRefresh = new System.Windows.Forms.Button();
      this.buttonLaunch = new System.Windows.Forms.Button();
      this.textBoxLastIp = new System.Windows.Forms.TextBox();
      this.listBoxIpAddresses = new System.Windows.Forms.ListBox();
      this.checkBoxSkip = new System.Windows.Forms.CheckBox();
      this.statusStrip1 = new System.Windows.Forms.StatusStrip();
      this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
      this.buttonEdit = new System.Windows.Forms.Button();
      this.buttonSetup = new System.Windows.Forms.Button();
      this.statusStrip1.SuspendLayout();
      this.SuspendLayout();
      // 
      // radioButtonLastIp
      // 
      this.radioButtonLastIp.AutoSize = true;
      this.radioButtonLastIp.Location = new System.Drawing.Point(12, 12);
      this.radioButtonLastIp.Name = "radioButtonLastIp";
      this.radioButtonLastIp.Size = new System.Drawing.Size(14, 13);
      this.radioButtonLastIp.TabIndex = 0;
      this.radioButtonLastIp.TabStop = true;
      this.radioButtonLastIp.UseVisualStyleBackColor = true;
      this.radioButtonLastIp.CheckedChanged += new System.EventHandler(this.radioButtonLastIp_CheckedChanged);
      this.radioButtonLastIp.MouseEnter += new System.EventHandler(this.textBoxLastIp_MouseEnter);
      this.radioButtonLastIp.MouseLeave += new System.EventHandler(this.tools_MouseLeave);
      // 
      // radioButtonSelectIp
      // 
      this.radioButtonSelectIp.AutoSize = true;
      this.radioButtonSelectIp.Location = new System.Drawing.Point(12, 43);
      this.radioButtonSelectIp.Name = "radioButtonSelectIp";
      this.radioButtonSelectIp.Size = new System.Drawing.Size(14, 13);
      this.radioButtonSelectIp.TabIndex = 2;
      this.radioButtonSelectIp.TabStop = true;
      this.radioButtonSelectIp.UseVisualStyleBackColor = true;
      this.radioButtonSelectIp.CheckedChanged += new System.EventHandler(this.radioButtonSelectIp_CheckedChanged);
      this.radioButtonSelectIp.MouseEnter += new System.EventHandler(this.listBoxIpAddresses_MouseEnter);
      this.radioButtonSelectIp.MouseLeave += new System.EventHandler(this.tools_MouseLeave);
      // 
      // buttonRefresh
      // 
      this.buttonRefresh.Image = ((System.Drawing.Image)(resources.GetObject("buttonRefresh.Image")));
      this.buttonRefresh.Location = new System.Drawing.Point(32, 38);
      this.buttonRefresh.Name = "buttonRefresh";
      this.buttonRefresh.Size = new System.Drawing.Size(23, 23);
      this.buttonRefresh.TabIndex = 3;
      this.buttonRefresh.UseVisualStyleBackColor = true;
      this.buttonRefresh.Click += new System.EventHandler(this.buttonRefresh_Click);
      this.buttonRefresh.MouseEnter += new System.EventHandler(this.buttonRefresh_MouseEnter);
      this.buttonRefresh.MouseLeave += new System.EventHandler(this.tools_MouseLeave);
      // 
      // buttonLaunch
      // 
      this.buttonLaunch.Location = new System.Drawing.Point(170, 38);
      this.buttonLaunch.Name = "buttonLaunch";
      this.buttonLaunch.Size = new System.Drawing.Size(92, 95);
      this.buttonLaunch.TabIndex = 9;
      this.buttonLaunch.Text = "Launch";
      this.buttonLaunch.UseVisualStyleBackColor = true;
      this.buttonLaunch.Click += new System.EventHandler(this.buttonLaunch_Click);
      this.buttonLaunch.MouseEnter += new System.EventHandler(this.buttonLaunch_MouseEnter);
      this.buttonLaunch.MouseLeave += new System.EventHandler(this.tools_MouseLeave);
      // 
      // textBoxLastIp
      // 
      this.textBoxLastIp.Location = new System.Drawing.Point(61, 9);
      this.textBoxLastIp.Name = "textBoxLastIp";
      this.textBoxLastIp.Size = new System.Drawing.Size(103, 20);
      this.textBoxLastIp.TabIndex = 1;
      this.textBoxLastIp.Click += new System.EventHandler(this.textBoxLastIp_Click);
      this.textBoxLastIp.TextChanged += new System.EventHandler(this.textBoxLastIp_TextChanged);
      this.textBoxLastIp.MouseEnter += new System.EventHandler(this.textBoxLastIp_MouseEnter);
      this.textBoxLastIp.MouseLeave += new System.EventHandler(this.tools_MouseLeave);
      // 
      // listBoxIpAddresses
      // 
      this.listBoxIpAddresses.FormattingEnabled = true;
      this.listBoxIpAddresses.Location = new System.Drawing.Point(61, 38);
      this.listBoxIpAddresses.Name = "listBoxIpAddresses";
      this.listBoxIpAddresses.Size = new System.Drawing.Size(103, 56);
      this.listBoxIpAddresses.TabIndex = 4;
      this.listBoxIpAddresses.SelectedIndexChanged += new System.EventHandler(this.listBoxIpAddresses_SelectedIndexChanged);
      this.listBoxIpAddresses.MouseEnter += new System.EventHandler(this.listBoxIpAddresses_MouseEnter);
      this.listBoxIpAddresses.MouseLeave += new System.EventHandler(this.tools_MouseLeave);
      // 
      // checkBoxSkip
      // 
      this.checkBoxSkip.AutoSize = true;
      this.checkBoxSkip.Checked = true;
      this.checkBoxSkip.CheckState = System.Windows.Forms.CheckState.Checked;
      this.checkBoxSkip.Location = new System.Drawing.Point(182, 12);
      this.checkBoxSkip.Name = "checkBoxSkip";
      this.checkBoxSkip.Size = new System.Drawing.Size(70, 17);
      this.checkBoxSkip.TabIndex = 8;
      this.checkBoxSkip.Text = "Skip intro";
      this.checkBoxSkip.UseVisualStyleBackColor = true;
      this.checkBoxSkip.CheckedChanged += new System.EventHandler(this.checkBoxSkip_CheckedChanged);
      this.checkBoxSkip.MouseEnter += new System.EventHandler(this.checkBoxSkip_MouseEnter);
      this.checkBoxSkip.MouseLeave += new System.EventHandler(this.tools_MouseLeave);
      // 
      // statusStrip1
      // 
      this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
      this.statusStrip1.Location = new System.Drawing.Point(0, 144);
      this.statusStrip1.Name = "statusStrip1";
      this.statusStrip1.Size = new System.Drawing.Size(274, 22);
      this.statusStrip1.SizingGrip = false;
      this.statusStrip1.TabIndex = 11;
      // 
      // toolStripStatusLabel1
      // 
      this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
      this.toolStripStatusLabel1.Size = new System.Drawing.Size(39, 17);
      this.toolStripStatusLabel1.Text = "Ready";
      // 
      // buttonEdit
      // 
      this.buttonEdit.Image = ((System.Drawing.Image)(resources.GetObject("buttonEdit.Image")));
      this.buttonEdit.Location = new System.Drawing.Point(89, 100);
      this.buttonEdit.Name = "buttonEdit";
      this.buttonEdit.Size = new System.Drawing.Size(75, 33);
      this.buttonEdit.TabIndex = 7;
      this.buttonEdit.UseVisualStyleBackColor = true;
      this.buttonEdit.Click += new System.EventHandler(this.buttonEdit_Click);
      this.buttonEdit.MouseEnter += new System.EventHandler(this.buttonEdit_MouseEnter);
      this.buttonEdit.MouseLeave += new System.EventHandler(this.tools_MouseLeave);
      // 
      // buttonSetup
      // 
      this.buttonSetup.Image = ((System.Drawing.Image)(resources.GetObject("buttonSetup.Image")));
      this.buttonSetup.Location = new System.Drawing.Point(32, 100);
      this.buttonSetup.Name = "buttonSetup";
      this.buttonSetup.Size = new System.Drawing.Size(51, 33);
      this.buttonSetup.TabIndex = 12;
      this.buttonSetup.UseVisualStyleBackColor = true;
      this.buttonSetup.Click += new System.EventHandler(this.buttonSetup_Click);
      this.buttonSetup.MouseEnter += new System.EventHandler(this.buttonSetup_MouseEnter);
      this.buttonSetup.MouseLeave += new System.EventHandler(this.tools_MouseLeave);
      // 
      // FormMain
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(274, 166);
      this.Controls.Add(this.buttonSetup);
      this.Controls.Add(this.buttonEdit);
      this.Controls.Add(this.statusStrip1);
      this.Controls.Add(this.checkBoxSkip);
      this.Controls.Add(this.listBoxIpAddresses);
      this.Controls.Add(this.textBoxLastIp);
      this.Controls.Add(this.buttonLaunch);
      this.Controls.Add(this.buttonRefresh);
      this.Controls.Add(this.radioButtonSelectIp);
      this.Controls.Add(this.radioButtonLastIp);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.MaximizeBox = false;
      this.Name = "FormMain";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "aoe3launcher";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
      this.Load += new System.EventHandler(this.Form1_Load);
      this.statusStrip1.ResumeLayout(false);
      this.statusStrip1.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.RadioButton radioButtonLastIp;
    private System.Windows.Forms.RadioButton radioButtonSelectIp;
    private System.Windows.Forms.Button buttonRefresh;
    private System.Windows.Forms.Button buttonLaunch;
    private System.Windows.Forms.TextBox textBoxLastIp;
    private System.Windows.Forms.ListBox listBoxIpAddresses;
    private System.Windows.Forms.CheckBox checkBoxSkip;
    private System.Windows.Forms.StatusStrip statusStrip1;
    private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
    private System.Windows.Forms.Button buttonEdit;
    private System.Windows.Forms.Button buttonSetup;
  }
}

